# HIBP
CakePHP plugin for checking passwords against the HIBP API

# Usage
Install using composer and use the class as-is... nothing major really

> composer require finlaydag33k\hibp

```php

use FinlayDaG33k\HIBP\HIBP;

// Prepare the password
$password = "lamepassword";
$hash = Security::hash($password, 'sha1', false);
$sub5 = substr($hash, 0, 5);

// Get a list of hashes
$hashes = (new HIBP())->checkPassword($sub5);

// Check if the "remainder" of the hash exists in our dataset
$results = array_filter($hashes, function ($row) use ($hashes, $hash) {
  return strpos($row, strtoupper(substr($hash, 5))) !== false;
});
$compromised = (count($results) > 0);
var_dump($compromised); // bool(true)
```